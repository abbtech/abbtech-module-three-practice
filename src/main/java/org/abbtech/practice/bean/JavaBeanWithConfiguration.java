package org.abbtech.practice.bean;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.annotation.RequestScope;

@Configuration
public class JavaBeanWithConfiguration {

    @Bean("Stundent1")
    @Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public Student getStudent() {
        Student student = new Student();
        student.name = "Value";
        return student;
    }

    @Bean("Stundent12")
    @Scope(value = WebApplicationContext.SCOPE_REQUEST, proxyMode = ScopedProxyMode.TARGET_CLASS)
    public Student getStudent1() {
        Student student = new Student();
        student.name = "Value1";
        return student;
    }
}
