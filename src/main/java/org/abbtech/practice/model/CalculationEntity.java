package org.abbtech.practice.model;

import java.math.BigDecimal;

public class CalculationEntity {
    private Long id;
    private BigDecimal variableA;
    private BigDecimal variableB;
    private BigDecimal calcResult;
    private String calcMethod;

    public CalculationEntity(Long id, BigDecimal variableA, BigDecimal variableB, BigDecimal calcResult, String calcMethod) {
        this.id = id;
        this.variableA = variableA;
        this.variableB = variableB;
        this.calcResult = calcResult;
        this.calcMethod = calcMethod;
    }

    public CalculationEntity(BigDecimal variableA, BigDecimal variableB, BigDecimal calcResult, String calcMethod) {
        this.variableA = variableA;
        this.variableB = variableB;
        this.calcResult = calcResult;
        this.calcMethod = calcMethod;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getVariableA() {
        return variableA;
    }

    public void setVariableA(BigDecimal variableA) {
        this.variableA = variableA;
    }

    public BigDecimal getVariableB() {
        return variableB;
    }

    public void setVariableB(BigDecimal variableB) {
        this.variableB = variableB;
    }

    public BigDecimal getCalcResult() {
        return calcResult;
    }

    public void setCalcResult(BigDecimal calcResult) {
        this.calcResult = calcResult;
    }

    public String getCalcMethod() {
        return calcMethod;
    }

    public void setCalcMethod(String calcMethod) {
        this.calcMethod = calcMethod;
    }
}
