package org.abbtech.practice.repository;

import org.abbtech.practice.model.CalculationEntity;

import java.util.List;
import java.util.Optional;

public interface CalculationRepository {

    List<CalculationEntity> getCalculations();

    Optional<CalculationEntity> getCalculationyId(long id);

    void insert(CalculationEntity calculationEntity);

    void insertCalculation(List<CalculationEntity> calculationEntities);
}
