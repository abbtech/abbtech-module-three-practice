package org.abbtech.practice.controller;

import org.abbtech.practice.bean.ConfigFromProperties;
import org.abbtech.practice.bean.JavaBeanWithAnnotation;
import org.abbtech.practice.dto.CalculationRespDTO;
import org.abbtech.practice.dto.StudentDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.annotation.RequestScope;

import java.math.BigDecimal;

@RestController
@RequestMapping("/sample")
@RequestScope
public class SampleController {

    @Autowired
    private JavaBeanWithAnnotation student;

    @Autowired
    private ConfigFromProperties configFromProperties;

    @GetMapping("/get/{variable-name-1}/{variable-name-2}")
    public void sampleGet(@PathVariable(value = "variable-name-1", required = false) String variable1,
                          @PathVariable("variable-name-2") Integer variable2,
                          @RequestParam("variable-name-3") String variable) {
        student.name = "GET METHOD";
        System.out.println(this.hashCode());
        System.out.println(student.name);
        System.out.println(configFromProperties.getName());
    }

    @PostMapping("/post")
    public CalculationRespDTO samplePost(@RequestBody StudentDTO student) {
        System.out.println(this.student.name);
        System.out.println(this.hashCode());
        return new CalculationRespDTO(BigDecimal.valueOf(12.6));
    }
}
