package org.abbtech.practice.bean;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class MailNotificationService implements NotificationService {

    private final Student student;

    @Autowired
    public MailNotificationService(@Qualifier("Stundent12") Student student) {
        this.student = student;
    }

    @Override
    public void sendNotification() {
        System.out.println("send mail notification" + student.name);
    }
}
