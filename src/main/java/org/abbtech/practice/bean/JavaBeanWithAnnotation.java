package org.abbtech.practice.bean;


import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.RequestScope;

@Component(value = "JavaBeanWithAnnotation")
@RequestScope
public class JavaBeanWithAnnotation {
    public String name = "Value";
}
