package org.abbtech.practice;

import org.abbtech.practice.bean.Student;
import org.abbtech.practice.model.CalculationEntity;
import org.abbtech.practice.repository.CalculationRepository;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.math.BigDecimal;
import java.util.Arrays;

@SpringBootApplication
public class PracticeApplication {
    public static void main(String[] args) {
        var context = SpringApplication.run(PracticeApplication.class, args);
        Student student = (Student) context.getBean("Stundent1");
        Student student1 = (Student) context.getBean("Stundent1");
        System.out.println(student1.hashCode());
        System.out.println(student.hashCode());
        var calculationRepository = context.getBean(CalculationRepository.class);
        var calculations = calculationRepository.getCalculations();
        var calculation = calculationRepository.getCalculationyId(8);
        calculationRepository.insert(new CalculationEntity(BigDecimal.valueOf(15),
                BigDecimal.valueOf(15),
                BigDecimal.valueOf(30),
                "ADD"));
        calculationRepository.insertCalculation(Arrays.asList(new CalculationEntity(BigDecimal.valueOf(15),
                        BigDecimal.valueOf(15),
                        BigDecimal.valueOf(30),
                        "ADD"),
                new CalculationEntity(BigDecimal.valueOf(80),
                        BigDecimal.valueOf(50),
                        BigDecimal.valueOf(30),
                        "SUBTRACT")));
        System.out.println(calculations);
    }

}
