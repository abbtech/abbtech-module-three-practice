package org.abbtech.practice.repository.impl;

import org.abbtech.practice.model.CalculationEntity;
import org.abbtech.practice.repository.CalculationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

@Repository
public class CalculationRepositoryImpl implements CalculationRepository {
    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public CalculationRepositoryImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public List<CalculationEntity> getCalculations() {
        return jdbcTemplate.query("SELECT * FROM CALCULATION_RESULT",
                ((rs, rowNum) -> new CalculationEntity(rs.getLong("id"),
                        rs.getBigDecimal("variable_a"), rs.getBigDecimal("variable_b"),
                        rs.getBigDecimal("calc_result"), rs.getString("calc_method"))));
    }

    @Override
    public Optional<CalculationEntity> getCalculationyId(long id) {
        String sql = "SELECT * FROM CALCULATION_RESULT WHERE id = ?";
        CalculationEntity film = jdbcTemplate.queryForObject(sql,
                (rs, rowNum) -> new CalculationEntity(rs.getLong("id"),
                        rs.getBigDecimal("variable_a"), rs.getBigDecimal("variable_b"),
                        rs.getBigDecimal("calc_result"), rs.getString("calc_method")), id);
        return Optional.ofNullable(film);
    }

    @Override
    public void insert(CalculationEntity calculationEntity) {
        jdbcTemplate.update(
                """
                        INSERT INTO CALCULATION_RESULT(variable_a,variable_b,calc_result,calc_method)
                        VALUES (?,?,?,?)
                        """, calculationEntity.getVariableA(),
                calculationEntity.getVariableB(),
                calculationEntity.getCalcResult(),
                calculationEntity.getCalcMethod());
    }

    @Override
    public void insertCalculation(List<CalculationEntity> calculationEntities) {
        jdbcTemplate.batchUpdate("""
                    
                INSERT INTO CALCULATION_RESULT(variable_a,variable_b,calc_result,calc_method)
                        VALUES (?,?,?,?)
                         """, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                ps.setBigDecimal(1, calculationEntities.get(i).getVariableA());
                ps.setBigDecimal(2, calculationEntities.get(i).getVariableB());
                ps.setBigDecimal(3, calculationEntities.get(i).getCalcResult());
                ps.setString(4, calculationEntities.get(i).getCalcMethod());
            }

            @Override
            public int getBatchSize() {
                return calculationEntities.size();
            }
        });
    }
}
